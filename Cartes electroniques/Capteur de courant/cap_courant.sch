EESchema Schematic File Version 2  date 22/02/2013 18:25:12
LIBS:cksr25-np
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:cap_courant-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "22 feb 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4000 4250 3950 4250
Wire Wire Line
	3950 4250 3950 2850
Wire Wire Line
	3950 2850 3900 2850
Wire Wire Line
	3800 4250 3750 4250
Wire Wire Line
	3750 4250 3750 2850
Wire Wire Line
	3750 2850 3700 2850
Connection ~ 6150 3300
Connection ~ 6150 3400
Connection ~ 6150 3500
Connection ~ 6150 3600
Wire Wire Line
	6150 3600 6150 3750
Wire Wire Line
	6150 3750 5150 3750
Connection ~ 6050 3400
Wire Wire Line
	6050 3600 6050 3400
Wire Wire Line
	5150 3300 6150 3300
Connection ~ 3900 2850
Connection ~ 5150 3750
Connection ~ 5150 3600
Connection ~ 5150 3450
Connection ~ 5150 3300
Connection ~ 3700 2850
Connection ~ 3800 2850
Connection ~ 3900 2850
Connection ~ 4000 2850
Connection ~ 4000 4250
Connection ~ 3900 4250
Connection ~ 3800 4250
Connection ~ 3700 4250
Wire Wire Line
	3000 2800 4000 2800
Wire Wire Line
	4000 2800 4000 2850
Wire Wire Line
	5150 3450 5400 3450
Wire Wire Line
	5400 3450 5400 3400
Wire Wire Line
	5400 3400 6150 3400
Wire Wire Line
	5650 3600 5650 3500
Wire Wire Line
	5650 3500 6150 3500
Wire Wire Line
	3000 4250 3700 4250
Wire Wire Line
	3800 2850 3850 2850
Wire Wire Line
	3850 2850 3850 4250
Wire Wire Line
	3850 4250 3900 4250
$Comp
L C C1
U 1 1 51136538
P 5850 3600
F 0 "C1" H 5900 3700 50  0000 L CNN
F 1 "C" H 5900 3500 50  0000 L CNN
	1    5850 3600
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 5113652D
P 5400 3600
F 0 "R1" V 5480 3600 50  0000 C CNN
F 1 "1K" V 5400 3600 50  0000 C CNN
	1    5400 3600
	0    -1   -1   0   
$EndComp
$Comp
L CONN_1 P3
U 1 1 50A65313
P 3000 4100
F 0 "P3" H 3080 4100 40  0000 L CNN
F 1 "CONN_1" H 3000 4155 30  0001 C CNN
	1    3000 4100
	0    -1   -1   0   
$EndComp
$Comp
L CONN_1 P2
U 1 1 50A65306
P 3000 2650
F 0 "P2" H 3080 2650 40  0000 L CNN
F 1 "CONN_1" H 3000 2705 30  0001 C CNN
	1    3000 2650
	0    -1   -1   0   
$EndComp
$Comp
L CKSR25-NP U1
U 1 1 50A64876
P 4250 3500
F 0 "U1" V 4250 3500 60  0000 C CNN
F 1 "CKSR25-NP" V 4400 3400 60  0000 C CNN
	1    4250 3500
	0    1    1    0   
$EndComp
$Comp
L CONN_4 P1
U 1 1 50A6401D
P 6500 3450
F 0 "P1" V 6450 3450 50  0000 C CNN
F 1 "CONN_4" V 6550 3450 50  0000 C CNN
	1    6500 3450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
