EESchema Schematic File Version 2  date 10/04/2013 09:45:23
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:carte_generale-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "22 feb 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 9350 4200
Wire Wire Line
	9350 3700 9350 3665
Wire Wire Line
	9625 4175 9625 3670
Wire Wire Line
	8140 4300 8140 4725
Connection ~ 8780 4725
Wire Wire Line
	9625 4575 9625 4725
Wire Wire Line
	9625 4725 8140 4725
Connection ~ 9350 3670
Connection ~ 8140 3665
Wire Wire Line
	8095 3665 8380 3665
Wire Wire Line
	8140 3665 8140 3900
Connection ~ 7880 6320
Wire Wire Line
	7880 6320 7985 6320
Wire Wire Line
	7880 5795 7880 5755
Wire Wire Line
	8650 1850 8675 1850
Wire Wire Line
	8100 1950 8150 1950
Wire Wire Line
	1500 800  1500 850 
Connection ~ 17000 150 
Wire Wire Line
	5800 900  5600 900 
Wire Wire Line
	7475 2800 7750 2800
Wire Wire Line
	7475 2600 7750 2600
Wire Wire Line
	7225 2700 6900 2700
Wire Wire Line
	10650 1400 7150 1400
Wire Wire Line
	850  1900 850  1300
Wire Wire Line
	8100 1750 8150 1750
Wire Wire Line
	5800 6300 5750 6300
Wire Wire Line
	5800 6100 5750 6100
Wire Wire Line
	5800 5900 5750 5900
Wire Wire Line
	5800 5700 5750 5700
Wire Wire Line
	5800 5400 5750 5400
Wire Wire Line
	5800 5200 5750 5200
Wire Wire Line
	5800 5000 5750 5000
Wire Wire Line
	5800 4800 5750 4800
Wire Wire Line
	5800 4500 5750 4500
Wire Wire Line
	5800 4300 5750 4300
Wire Wire Line
	5800 4100 5750 4100
Wire Wire Line
	5800 3900 5750 3900
Wire Wire Line
	5800 3300 5750 3300
Wire Wire Line
	5800 3500 5750 3500
Wire Wire Line
	5800 3200 5750 3200
Wire Wire Line
	5800 3000 5750 3000
Wire Wire Line
	5800 2700 5750 2700
Wire Wire Line
	5800 2500 5750 2500
Wire Wire Line
	5800 2300 5750 2300
Wire Wire Line
	5800 2100 5750 2100
Wire Wire Line
	1500 5500 1550 5500
Wire Wire Line
	1500 5300 1550 5300
Wire Wire Line
	1500 5100 1550 5100
Wire Wire Line
	1500 4900 1550 4900
Wire Wire Line
	1500 4600 1550 4600
Wire Wire Line
	1500 4400 1550 4400
Wire Wire Line
	1500 4200 1550 4200
Wire Wire Line
	1500 4000 1550 4000
Wire Wire Line
	1500 3700 1550 3700
Wire Wire Line
	1500 3500 1550 3500
Wire Wire Line
	1500 3300 1550 3300
Wire Wire Line
	1500 3100 1550 3100
Wire Wire Line
	1500 2800 1550 2800
Wire Wire Line
	1500 2600 1550 2600
Wire Wire Line
	1500 2400 1550 2400
Wire Wire Line
	1500 2100 1550 2100
Wire Wire Line
	2350 4000 2300 4000
Wire Wire Line
	5800 1400 5750 1400
Wire Wire Line
	5800 1200 5750 1200
Wire Wire Line
	5800 1800 5750 1800
Wire Wire Line
	5800 1600 5750 1600
Connection ~ 10250 1000
Connection ~ 7900 1000
Connection ~ 9900 1400
Connection ~ 7550 1400
Wire Wire Line
	7300 1950 7250 1950
Wire Wire Line
	4950 4100 5000 4100
Wire Wire Line
	4950 5200 5000 5200
Wire Wire Line
	4950 4800 5000 4800
Wire Wire Line
	2350 3000 2300 3000
Wire Wire Line
	2350 4900 2300 4900
Wire Wire Line
	2350 1200 2300 1200
Wire Wire Line
	2350 1800 2300 1800
Wire Wire Line
	2350 2200 2300 2200
Wire Wire Line
	2350 2400 2300 2400
Wire Wire Line
	2350 2600 2300 2600
Wire Wire Line
	2350 2800 2300 2800
Wire Wire Line
	2350 3200 2300 3200
Wire Wire Line
	2350 3400 2300 3400
Wire Wire Line
	2350 3600 2300 3600
Wire Wire Line
	2350 4100 2300 4100
Wire Wire Line
	2350 4300 2300 4300
Wire Wire Line
	2350 4500 2300 4500
Wire Wire Line
	2350 5000 2300 5000
Wire Wire Line
	2350 5200 2300 5200
Wire Wire Line
	2350 5400 2300 5400
Wire Wire Line
	2350 5700 2300 5700
Wire Wire Line
	2350 5900 2300 5900
Wire Wire Line
	2350 6100 2300 6100
Wire Wire Line
	4950 6400 5000 6400
Wire Wire Line
	4950 6200 5000 6200
Wire Wire Line
	4950 6000 5000 6000
Wire Wire Line
	4950 5800 5000 5800
Wire Wire Line
	4950 5500 5000 5500
Wire Wire Line
	4950 5100 5000 5100
Wire Wire Line
	4950 4400 5000 4400
Wire Wire Line
	4950 4600 5000 4600
Wire Wire Line
	4950 3700 5000 3700
Wire Wire Line
	4950 3900 5000 3900
Wire Wire Line
	4950 3400 5000 3400
Wire Wire Line
	4950 3200 5000 3200
Wire Wire Line
	4950 3000 5000 3000
Wire Wire Line
	4950 2700 5000 2700
Wire Wire Line
	4950 2500 5000 2500
Wire Wire Line
	4950 2300 5000 2300
Wire Wire Line
	5000 2100 4950 2100
Wire Wire Line
	4950 1800 5000 1800
Wire Wire Line
	4950 1600 5000 1600
Wire Wire Line
	4950 1400 5000 1400
Wire Wire Line
	4950 1200 5000 1200
Connection ~ 10150 3000
Wire Wire Line
	9550 3000 10900 3000
Connection ~ 10650 3000
Connection ~ 10650 2600
Wire Wire Line
	10900 2600 9550 2600
Connection ~ 10150 2600
Connection ~ 9900 1000
Wire Wire Line
	6600 5050 6500 5050
Wire Wire Line
	6600 4850 6500 4850
Wire Wire Line
	6600 4750 6500 4750
Wire Wire Line
	6600 4950 6500 4950
Wire Wire Line
	6600 5150 6500 5150
Wire Wire Line
	9400 6115 9300 6115
Connection ~ 8200 1000
Connection ~ 8850 1400
Connection ~ 9450 1400
Connection ~ 9450 1000
Connection ~ 10400 2600
Connection ~ 10400 3000
Wire Wire Line
	4950 1300 5000 1300
Wire Wire Line
	4950 1500 5000 1500
Wire Wire Line
	4950 1700 5000 1700
Wire Wire Line
	4950 1900 5000 1900
Wire Wire Line
	4950 2200 5000 2200
Wire Wire Line
	4950 2400 5000 2400
Wire Wire Line
	4950 2600 5000 2600
Wire Wire Line
	4950 2800 5000 2800
Wire Wire Line
	4950 3100 5000 3100
Wire Wire Line
	4950 3300 5000 3300
Wire Wire Line
	4950 3500 5000 3500
Wire Wire Line
	4950 4000 5000 4000
Wire Wire Line
	4950 3600 5000 3600
Wire Wire Line
	4950 4500 5000 4500
Wire Wire Line
	4950 4300 5000 4300
Wire Wire Line
	4950 5400 5000 5400
Wire Wire Line
	4950 5700 5000 5700
Wire Wire Line
	4950 5900 5000 5900
Wire Wire Line
	4950 6100 5000 6100
Wire Wire Line
	4950 6300 5000 6300
Wire Wire Line
	2350 6200 2300 6200
Wire Wire Line
	2350 6000 2300 6000
Wire Wire Line
	2350 5800 2300 5800
Wire Wire Line
	2350 5500 2300 5500
Wire Wire Line
	2350 5300 2300 5300
Wire Wire Line
	2350 5100 2300 5100
Wire Wire Line
	2350 4600 2300 4600
Wire Wire Line
	2350 4400 2300 4400
Wire Wire Line
	2350 4200 2300 4200
Wire Wire Line
	2350 3700 2300 3700
Wire Wire Line
	2350 3500 2300 3500
Wire Wire Line
	2350 3300 2300 3300
Wire Wire Line
	2350 3100 2300 3100
Wire Wire Line
	2350 2700 2300 2700
Wire Wire Line
	2350 2500 2300 2500
Wire Wire Line
	2350 2300 2300 2300
Wire Wire Line
	2350 2100 2300 2100
Wire Wire Line
	2350 1500 2300 1500
Wire Wire Line
	2350 4800 2300 4800
Wire Wire Line
	4950 5000 5000 5000
Wire Wire Line
	4950 4900 5000 4900
Wire Wire Line
	4950 5300 5000 5300
Wire Wire Line
	4950 4200 5000 4200
Connection ~ 8200 1400
Connection ~ 7900 1400
Connection ~ 10250 1400
Wire Wire Line
	8850 1400 8850 1300
Connection ~ 7550 1000
Wire Wire Line
	8450 1000 7150 1000
Wire Wire Line
	5800 1500 5750 1500
Wire Wire Line
	5800 1700 5750 1700
Wire Wire Line
	5800 1900 5750 1900
Wire Wire Line
	5800 1300 5750 1300
Wire Wire Line
	2350 3900 2300 3900
Wire Wire Line
	1500 2200 1550 2200
Wire Wire Line
	1500 2300 1550 2300
Wire Wire Line
	1500 2500 1550 2500
Wire Wire Line
	1500 2700 1550 2700
Wire Wire Line
	1500 3000 1550 3000
Wire Wire Line
	1500 3200 1550 3200
Wire Wire Line
	1500 3400 1550 3400
Wire Wire Line
	1500 3600 1550 3600
Wire Wire Line
	1500 3900 1550 3900
Wire Wire Line
	1500 4100 1550 4100
Wire Wire Line
	1500 4300 1550 4300
Wire Wire Line
	1500 4500 1550 4500
Wire Wire Line
	1500 4800 1550 4800
Wire Wire Line
	1500 5000 1550 5000
Wire Wire Line
	1500 5200 1550 5200
Wire Wire Line
	1500 5400 1550 5400
Wire Wire Line
	5800 2200 5750 2200
Wire Wire Line
	5800 2400 5750 2400
Wire Wire Line
	5800 2600 5750 2600
Wire Wire Line
	5800 2800 5750 2800
Wire Wire Line
	5800 3100 5750 3100
Wire Wire Line
	5800 3600 5750 3600
Wire Wire Line
	5800 3400 5750 3400
Wire Wire Line
	5800 3700 5750 3700
Wire Wire Line
	5800 4000 5750 4000
Wire Wire Line
	5800 4200 5750 4200
Wire Wire Line
	5800 4400 5750 4400
Wire Wire Line
	5800 4600 5750 4600
Wire Wire Line
	5800 4900 5750 4900
Wire Wire Line
	5800 5100 5750 5100
Wire Wire Line
	5800 5300 5750 5300
Wire Wire Line
	5800 5500 5750 5500
Wire Wire Line
	5800 5800 5750 5800
Wire Wire Line
	5800 6000 5750 6000
Wire Wire Line
	5800 6200 5750 6200
Wire Wire Line
	5800 6400 5750 6400
Wire Wire Line
	8100 1850 8150 1850
Wire Wire Line
	1550 1300 1250 1300
Connection ~ 1500 1300
Wire Wire Line
	1550 1900 1250 1900
Connection ~ 1500 1900
Wire Wire Line
	850  1550 800  1550
Connection ~ 850  1550
Wire Wire Line
	7225 2600 6900 2600
Wire Wire Line
	7225 2800 6900 2800
Wire Wire Line
	7750 2700 7475 2700
Wire Wire Line
	10650 1000 9250 1000
Wire Wire Line
	5575 700  5800 700 
Wire Wire Line
	1500 850  1575 850 
Wire Wire Line
	8650 1750 8675 1750
Wire Wire Line
	8650 1950 8675 1950
Wire Wire Line
	1975 850  2050 850 
Wire Wire Line
	7880 6345 7880 6295
Wire Wire Line
	9400 6215 9305 6215
Wire Wire Line
	8780 4225 8780 4165
Wire Wire Line
	9350 3665 9180 3665
Wire Wire Line
	9350 3670 9770 3670
Connection ~ 9625 3670
Wire Wire Line
	8780 4725 8780 4770
Wire Wire Line
	9350 4200 8780 4200
Connection ~ 8780 4200
$Comp
L +3,3V #PWR01
U 1 1 510B85CE
P 9400 6015
F 0 "#PWR01" H 9400 5975 30  0001 C CNN
F 1 "+3,3V" H 9400 6125 30  0000 C CNN
	1    9400 6015
	1    0    0    -1  
$EndComp
$Comp
L +3,3V #PWR02
U 1 1 510B85C5
P 9770 3670
F 0 "#PWR02" H 9770 3630 30  0001 C CNN
F 1 "+3,3V" H 9770 3780 30  0000 C CNN
	1    9770 3670
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 510B85A8
P 8095 3665
F 0 "#PWR03" H 8095 3755 20  0001 C CNN
F 1 "+5V" H 8095 3755 30  0000 C CNN
	1    8095 3665
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 510B85A2
P 8780 4770
F 0 "#PWR04" H 8780 4770 30  0001 C CNN
F 1 "GND" H 8780 4700 30  0001 C CNN
	1    8780 4770
	1    0    0    -1  
$EndComp
$Comp
L CP1 C21
U 1 1 510B8533
P 9625 4375
F 0 "C21" H 9675 4475 50  0000 L CNN
F 1 "1µF" H 9675 4275 50  0000 L CNN
	1    9625 4375
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 510B851E
P 9350 3950
F 0 "R7" V 9430 3950 50  0000 C CNN
F 1 "240Ω" V 9350 3950 50  0000 C CNN
	1    9350 3950
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 510B851A
P 8780 4475
F 0 "R8" V 8860 4475 50  0000 C CNN
F 1 "390Ω" V 8780 4475 50  0000 C CNN
	1    8780 4475
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 510B8511
P 8140 4100
F 0 "C20" H 8190 4200 50  0000 L CNN
F 1 "100nF" H 8190 4000 50  0000 L CNN
	1    8140 4100
	1    0    0    -1  
$EndComp
Text Label 9300 6115 2    60   ~ 0
RX1
Text Label 7985 6320 0    60   ~ 0
RX1
$Comp
L LM317 U2
U 1 1 510B81A4
P 8780 3815
F 0 "U2" H 8780 4115 60  0000 C CNN
F 1 "LM317" H 8830 3565 60  0000 L CNN
	1    8780 3815
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 510B79BD
P 7880 5755
F 0 "#PWR05" H 7880 5845 20  0001 C CNN
F 1 "+5V" H 7880 5845 30  0000 C CNN
	1    7880 5755
	1    0    0    -1  
$EndComp
NoConn ~ 2300 6200
NoConn ~ 2300 6100
NoConn ~ 2300 6000
NoConn ~ 2300 5900
NoConn ~ 2300 5800
NoConn ~ 2300 5700
Text Label 8675 1950 0    60   ~ 0
SCK
Text Label 8675 1850 0    60   ~ 0
MISO
Text Label 8675 1750 0    60   ~ 0
MOSI
$Comp
L R R4
U 1 1 50F9662E
P 8400 1950
F 0 "R4" V 8480 1950 50  0000 C CNN
F 1 "1KΩ" V 8400 1950 50  0000 C CNN
	1    8400 1950
	0    -1   -1   0   
$EndComp
$Comp
L R R5
U 1 1 50F9662B
P 8400 1850
F 0 "R5" V 8480 1850 50  0000 C CNN
F 1 "1KΩ" V 8400 1850 50  0000 C CNN
	1    8400 1850
	0    -1   -1   0   
$EndComp
$Comp
L R R6
U 1 1 50F96627
P 8400 1750
F 0 "R6" V 8480 1750 50  0000 C CNN
F 1 "1KΩ" V 8400 1750 50  0000 C CNN
	1    8400 1750
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR06
U 1 1 50F952C8
P 1000 850
F 0 "#PWR06" H 1000 940 20  0001 C CNN
F 1 "+5V" H 1000 940 30  0000 C CNN
	1    1000 850 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 50F952AE
P 2050 850
F 0 "#PWR07" H 2050 850 30  0001 C CNN
F 1 "GND" H 2050 780 30  0001 C CNN
	1    2050 850 
	0    -1   -1   0   
$EndComp
Text Label 1500 800  1    60   ~ 0
RST
$Comp
L R R3
U 1 1 50F9528C
P 1250 850
F 0 "R3" V 1330 850 50  0000 C CNN
F 1 "1KΩ" V 1250 850 50  0000 C CNN
	1    1250 850 
	0    -1   -1   0   
$EndComp
$Comp
L C C19
U 1 1 50F95282
P 1775 850
F 0 "C19" H 1825 950 50  0000 L CNN
F 1 "C" H 1825 750 50  0000 L CNN
	1    1775 850 
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR08
U 1 1 50F95070
P 3800 6700
F 0 "#PWR08" H 3800 6700 30  0001 C CNN
F 1 "GND" H 3800 6630 30  0001 C CNN
	1    3800 6700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 50F9506E
P 3700 6700
F 0 "#PWR09" H 3700 6700 30  0001 C CNN
F 1 "GND" H 3700 6630 30  0001 C CNN
	1    3700 6700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 50F9506C
P 3600 6700
F 0 "#PWR010" H 3600 6700 30  0001 C CNN
F 1 "GND" H 3600 6630 30  0001 C CNN
	1    3600 6700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 50F95069
P 3500 6700
F 0 "#PWR011" H 3500 6700 30  0001 C CNN
F 1 "GND" H 3500 6630 30  0001 C CNN
	1    3500 6700
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG012
U 1 1 50F95045
P 5600 900
F 0 "#FLG012" H 5600 995 30  0001 C CNN
F 1 "PWR_FLAG" H 5600 1080 30  0000 C CNN
	1    5600 900 
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG013
U 1 1 50F95041
P 5575 700
F 0 "#FLG013" H 5575 795 30  0001 C CNN
F 1 "PWR_FLAG" H 5575 880 30  0000 C CNN
	1    5575 700 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 50F94FAA
P 7300 1850
F 0 "#PWR014" H 7300 1850 30  0001 C CNN
F 1 "GND" H 7300 1780 30  0001 C CNN
	1    7300 1850
	0    1    1    0   
$EndComp
$Comp
L GND #PWR015
U 1 1 50F94F15
P 7300 2050
F 0 "#PWR015" H 7300 2050 30  0001 C CNN
F 1 "GND" H 7300 1980 30  0001 C CNN
	1    7300 2050
	0    1    1    0   
$EndComp
Text Label 1550 5500 0    60   ~ 0
RX2
Text Label 1550 5400 0    60   ~ 0
TX2
Text Label 1550 3700 0    60   ~ 0
Vout
$Comp
L GND #PWR016
U 1 1 50F94E57
P 5800 900
F 0 "#PWR016" H 5800 900 30  0001 C CNN
F 1 "GND" H 5800 830 30  0001 C CNN
	1    5800 900 
	0    1    1    0   
$EndComp
Text Label 5800 700  2    60   ~ 0
VSS
$Comp
L CONN_2 P16
U 1 1 50F94D73
P 6150 800
F 0 "P16" V 6100 800 40  0000 C CNN
F 1 "Alim" V 6200 800 40  0000 C CNN
	1    6150 800 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 50F94D5F
P 10650 1000
F 0 "#PWR017" H 10650 1090 20  0001 C CNN
F 1 "+5V" H 10650 1090 30  0000 C CNN
	1    10650 1000
	1    0    0    -1  
$EndComp
Text Label 7150 1000 2    60   ~ 0
VSS
Text Label 1550 1900 0    60   ~ 0
XTAL1
Text Label 1550 1300 0    60   ~ 0
XTAL2
$Comp
L GND #PWR018
U 1 1 50F94870
P 800 1550
F 0 "#PWR018" H 800 1550 30  0001 C CNN
F 1 "GND" H 800 1480 30  0001 C CNN
	1    800  1550
	0    1    1    0   
$EndComp
$Comp
L C C17
U 1 1 50F94849
P 1050 1900
F 0 "C17" H 1100 2000 50  0000 L CNN
F 1 "20pF" H 1100 1800 50  0000 L CNN
	1    1050 1900
	0    -1   -1   0   
$EndComp
$Comp
L C C16
U 1 1 50F94841
P 1050 1300
F 0 "C16" H 1100 1400 50  0000 L CNN
F 1 "20pF" H 1100 1200 50  0000 L CNN
	1    1050 1300
	0    -1   -1   0   
$EndComp
Text Label 1550 4600 0    60   ~ 0
RX3
Text Label 1550 4500 0    60   ~ 0
TX3
Text Label 5750 2200 2    60   ~ 0
SCK
Text Label 5750 2300 2    60   ~ 0
MOSI
Text Label 5750 2400 2    60   ~ 0
MISO
Text Label 5750 4200 2    60   ~ 0
TX1
Text Label 5750 4100 2    60   ~ 0
RX1
Text Label 5750 5300 2    60   ~ 0
RoueD
Text Label 5750 5200 2    60   ~ 0
RoueG
Text Label 5750 5000 2    60   ~ 0
RESET
Text Label 5750 4900 2    60   ~ 0
TX0
Text Label 5750 4800 2    60   ~ 0
RX0
$Comp
L CRYSTAL X1
U 1 1 50F946EF
P 1500 1600
F 0 "X1" H 1500 1750 60  0000 C CNN
F 1 "CRYSTAL" H 1500 1450 60  0000 C CNN
	1    1500 1600
	0    -1   -1   0   
$EndComp
NoConn ~ 10800 6915
NoConn ~ 10800 6815
NoConn ~ 10800 6715
NoConn ~ 10800 6615
NoConn ~ 10800 6515
NoConn ~ 10800 6415
NoConn ~ 10800 6315
NoConn ~ 10800 6215
NoConn ~ 10800 6115
NoConn ~ 10800 6015
Text Label 5750 6400 2    60   ~ 0
PF7
Text Label 5750 6300 2    60   ~ 0
PF6
Text Label 5750 6200 2    60   ~ 0
PF5
Text Label 5750 6100 2    60   ~ 0
PF4
Text Label 5750 6000 2    60   ~ 0
PF3
Text Label 5750 5900 2    60   ~ 0
PF2
Text Label 5750 5800 2    60   ~ 0
PF1
Text Label 5750 5700 2    60   ~ 0
PF0
Text Label 5750 5500 2    60   ~ 0
PE7
Text Label 5750 5400 2    60   ~ 0
PE6
Text Label 5750 5100 2    60   ~ 0
PE3
Text Label 5750 4600 2    60   ~ 0
PD7
Text Label 5750 4500 2    60   ~ 0
PD6
Text Label 5750 4400 2    60   ~ 0
PD5
Text Label 5750 4300 2    60   ~ 0
PD4
Text Label 5750 4000 2    60   ~ 0
PD1
Text Label 5750 3900 2    60   ~ 0
PD0
Text Label 5750 3700 2    60   ~ 0
PC7
Text Label 5750 3600 2    60   ~ 0
PC6
Text Label 5750 3500 2    60   ~ 0
PC5
Text Label 5750 3400 2    60   ~ 0
PC4
Text Label 5750 3300 2    60   ~ 0
PC3
Text Label 5750 3200 2    60   ~ 0
PC2
Text Label 5750 3100 2    60   ~ 0
PC1
Text Label 5750 3000 2    60   ~ 0
PC0
Text Label 5750 2800 2    60   ~ 0
PB7
Text Label 5750 2700 2    60   ~ 0
PB6
Text Label 5750 2600 2    60   ~ 0
PB5
Text Label 5750 2500 2    60   ~ 0
PB4
Text Label 5750 2100 2    60   ~ 0
PB0
Text Label 1550 5300 0    60   ~ 0
PH2
Text Label 1550 5200 0    60   ~ 0
PH3
Text Label 1550 5100 0    60   ~ 0
PH4
Text Label 1550 5000 0    60   ~ 0
PH5
Text Label 1550 4900 0    60   ~ 0
PH6
Text Label 1550 4800 0    60   ~ 0
PH7
Text Label 1550 4400 0    60   ~ 0
PJ2
Text Label 1550 4300 0    60   ~ 0
PJ3
Text Label 1550 4200 0    60   ~ 0
PJ4
Text Label 1550 4100 0    60   ~ 0
PJ5
Text Label 1550 4000 0    60   ~ 0
PJ6
Text Label 1550 3900 0    60   ~ 0
PJ7
Text Label 1550 3600 0    60   ~ 0
PK1
Text Label 1550 3500 0    60   ~ 0
PK2
Text Label 1550 3400 0    60   ~ 0
PK3
Text Label 1550 3300 0    60   ~ 0
PK4
Text Label 1550 3200 0    60   ~ 0
PK5
Text Label 1550 3100 0    60   ~ 0
PK6
Text Label 1550 3000 0    60   ~ 0
PK7
Text Label 1550 2800 0    60   ~ 0
PL0
Text Label 1550 2700 0    60   ~ 0
PL1
Text Label 1550 2600 0    60   ~ 0
PL2
Text Label 1550 2500 0    60   ~ 0
PL3
Text Label 1550 2400 0    60   ~ 0
PL4
Text Label 1550 2300 0    60   ~ 0
PL5
Text Label 1550 2200 0    60   ~ 0
PL6
Text Label 1550 2100 0    60   ~ 0
PL7
$Comp
L CONN_8 P12
U 1 1 50F94276
P 1150 5150
F 0 "P12" V 1100 5150 60  0000 C CNN
F 1 "PORTH" V 1200 5150 60  0000 C CNN
	1    1150 5150
	-1   0    0    1   
$EndComp
$Comp
L CONN_8 P13
U 1 1 50F94250
P 1150 4250
F 0 "P13" V 1100 4250 60  0000 C CNN
F 1 "PORTJ" V 1200 4250 60  0000 C CNN
	1    1150 4250
	-1   0    0    1   
$EndComp
$Comp
L CONN_8 P14
U 1 1 50F94248
P 1150 3350
F 0 "P14" V 1100 3350 60  0000 C CNN
F 1 "PORTK" V 1200 3350 60  0000 C CNN
	1    1150 3350
	-1   0    0    1   
$EndComp
$Comp
L CONN_8 P15
U 1 1 50F94239
P 1150 2450
F 0 "P15" V 1100 2450 60  0000 C CNN
F 1 "PORTL" V 1200 2450 60  0000 C CNN
	1    1150 2450
	-1   0    0    1   
$EndComp
$Comp
L CONN_8 P10
U 1 1 50F9422B
P 6150 6050
F 0 "P10" V 6100 6050 60  0000 C CNN
F 1 "PORTF" V 6200 6050 60  0000 C CNN
	1    6150 6050
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P9
U 1 1 50F94226
P 6150 5150
F 0 "P9" V 6100 5150 60  0000 C CNN
F 1 "PORTE" V 6200 5150 60  0000 C CNN
	1    6150 5150
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P8
U 1 1 50F94221
P 6150 4250
F 0 "P8" V 6100 4250 60  0000 C CNN
F 1 "PORTD" V 6200 4250 60  0000 C CNN
	1    6150 4250
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P7
U 1 1 50F9421C
P 6150 3350
F 0 "P7" V 6100 3350 60  0000 C CNN
F 1 "PORTC" V 6200 3350 60  0000 C CNN
	1    6150 3350
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P6
U 1 1 50F94206
P 6150 2450
F 0 "P6" V 6100 2450 60  0000 C CNN
F 1 "PORTB" V 6200 2450 60  0000 C CNN
	1    6150 2450
	1    0    0    -1  
$EndComp
Text Label 5750 1900 2    60   ~ 0
PA7
Text Label 5750 1800 2    60   ~ 0
PA6
Text Label 5750 1700 2    60   ~ 0
PA5
Text Label 5750 1600 2    60   ~ 0
PA4
Text Label 5750 1500 2    60   ~ 0
PA3
Text Label 5750 1400 2    60   ~ 0
PA2
Text Label 5750 1300 2    60   ~ 0
PA1
Text Label 5750 1200 2    60   ~ 0
PA0
$Comp
L CONN_8 P5
U 1 1 50F9176A
P 6150 1550
F 0 "P5" V 6100 1550 60  0000 C CNN
F 1 "PORTA" V 6200 1550 60  0000 C CNN
	1    6150 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 50F0466B
P 10650 1400
F 0 "#PWR019" H 10650 1400 30  0001 C CNN
F 1 "GND" H 10650 1330 30  0001 C CNN
	1    10650 1400
	0    -1   -1   0   
$EndComp
$Comp
L C C10
U 1 1 50F0460D
P 10250 1200
F 0 "C10" H 10300 1300 50  0000 L CNN
F 1 "C" H 10300 1100 50  0000 L CNN
	1    10250 1200
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 50F04604
P 7550 1200
F 0 "C9" H 7600 1300 50  0000 L CNN
F 1 "C" H 7600 1100 50  0000 L CNN
	1    7550 1200
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 50F045F2
P 7900 1200
F 0 "C8" H 7950 1300 50  0000 L CNN
F 1 "C" H 7950 1100 50  0000 L CNN
	1    7900 1200
	1    0    0    -1  
$EndComp
Text Label 7250 1950 2    60   ~ 0
Vout
NoConn ~ 7300 2150
NoConn ~ 8100 2150
NoConn ~ 8100 2050
Text Label 7750 2700 0    60   ~ 0
MOSI
Text Label 6900 2700 2    60   ~ 0
SCK
Text Label 6900 2600 2    60   ~ 0
MISO
Text Label 6900 2800 2    60   ~ 0
RST
$Comp
L +5V #PWR020
U 1 1 50F044F3
P 7750 2600
F 0 "#PWR020" H 7750 2690 20  0001 C CNN
F 1 "+5V" H 7750 2690 30  0000 C CNN
	1    7750 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 50F044EC
P 7750 2800
F 0 "#PWR021" H 7750 2800 30  0001 C CNN
F 1 "GND" H 7750 2730 30  0001 C CNN
	1    7750 2800
	0    -1   -1   0   
$EndComp
$Comp
L AVR-ISP-6 CON1
U 1 1 50F044CA
P 7350 2700
F 0 "CON1" H 7270 2940 50  0000 C CNN
F 1 "AVR-ISP-6" H 7110 2470 50  0000 L BNN
F 2 "AVR-ISP-6" V 6830 2740 50  0001 C CNN
	1    7350 2700
	1    0    0    -1  
$EndComp
Text Label 5000 6400 0    60   ~ 0
PF7
Text Label 5000 6300 0    60   ~ 0
PF6
Text Label 5000 6200 0    60   ~ 0
PF5
Text Label 5000 6100 0    60   ~ 0
PF4
Text Label 5000 6000 0    60   ~ 0
PF3
Text Label 5000 5900 0    60   ~ 0
PF2
Text Label 5000 5800 0    60   ~ 0
PF1
Text Label 5000 5700 0    60   ~ 0
PF0
Text Label 5000 5500 0    60   ~ 0
PE7
Text Label 5000 5400 0    60   ~ 0
PE6
Text Label 5000 5100 0    60   ~ 0
PE3
Text Label 5000 4600 0    60   ~ 0
PD7
Text Label 5000 4500 0    60   ~ 0
PD6
Text Label 5000 4400 0    60   ~ 0
PD5
Text Label 5000 4300 0    60   ~ 0
PD4
Text Label 5000 4000 0    60   ~ 0
PD1
Text Label 5000 3900 0    60   ~ 0
PD0
Text Label 5000 3700 0    60   ~ 0
PC7
Text Label 5000 3600 0    60   ~ 0
PC6
Text Label 5000 3500 0    60   ~ 0
PC5
Text Label 5000 3400 0    60   ~ 0
PC4
Text Label 5000 3300 0    60   ~ 0
PC3
Text Label 5000 3200 0    60   ~ 0
PC2
Text Label 5000 3100 0    60   ~ 0
PC1
Text Label 5000 3000 0    60   ~ 0
PC0
Text Label 5000 2800 0    60   ~ 0
PB7
Text Label 5000 2700 0    60   ~ 0
PB6
Text Label 5000 2600 0    60   ~ 0
PB5
Text Label 5000 2500 0    60   ~ 0
PB4
Text Label 5000 2400 0    60   ~ 0
MISO
Text Label 5000 2300 0    60   ~ 0
MOSI
Text Label 5000 2200 0    60   ~ 0
SCK
Text Label 5000 2100 0    60   ~ 0
PB0
Text Label 5000 1900 0    60   ~ 0
PA7
Text Label 5000 1800 0    60   ~ 0
PA6
Text Label 5000 1700 0    60   ~ 0
PA5
Text Label 5000 1600 0    60   ~ 0
PA4
Text Label 5000 1500 0    60   ~ 0
PA3
Text Label 5000 1400 0    60   ~ 0
PA2
Text Label 5000 1300 0    60   ~ 0
PA1
Text Label 5000 1200 0    60   ~ 0
PA0
Text Label 2300 3000 2    60   ~ 0
Vout
Text Label 2300 4900 2    60   ~ 0
TX2
Text Label 2300 4800 2    60   ~ 0
RX2
Text Label 2300 5500 2    60   ~ 0
PH7
Text Label 2300 5400 2    60   ~ 0
PH6
Text Label 2300 5300 2    60   ~ 0
PH5
Text Label 2300 5200 2    60   ~ 0
PH4
Text Label 2300 5100 2    60   ~ 0
PH3
Text Label 2300 5000 2    60   ~ 0
PH2
Text Label 2300 4600 2    60   ~ 0
PJ7
Text Label 2300 4500 2    60   ~ 0
PJ6
Text Label 2300 4400 2    60   ~ 0
PJ5
Text Label 2300 4300 2    60   ~ 0
PJ4
Text Label 2300 4200 2    60   ~ 0
PJ3
Text Label 2300 4100 2    60   ~ 0
PJ2
Text Label 2300 3700 2    60   ~ 0
PK7
Text Label 2300 3600 2    60   ~ 0
PK6
Text Label 2300 3500 2    60   ~ 0
PK5
Text Label 2300 3400 2    60   ~ 0
PK4
Text Label 2300 3300 2    60   ~ 0
PK3
Text Label 2300 3200 2    60   ~ 0
PK2
Text Label 2300 3100 2    60   ~ 0
PK1
Text Label 2300 2800 2    60   ~ 0
PL7
Text Label 2300 2700 2    60   ~ 0
PL6
Text Label 2300 2600 2    60   ~ 0
PL5
Text Label 2300 2500 2    60   ~ 0
PL4
Text Label 2300 2400 2    60   ~ 0
PL3
Text Label 2300 2300 2    60   ~ 0
PL2
Text Label 2300 2200 2    60   ~ 0
PL1
Text Label 2300 2100 2    60   ~ 0
PL0
Text Label 2300 1800 2    60   ~ 0
XTAL1
Text Label 2300 1500 2    60   ~ 0
XTAL2
Text Label 2300 1200 2    60   ~ 0
RST
$Comp
L ATMEGA1280-A IC1
U 1 1 50F00C3A
P 3650 3800
F 0 "IC1" H 2550 6550 50  0000 L BNN
F 1 "ATMEGA1280-A" H 4150 1000 50  0000 L BNN
F 2 "TQFP100" H 2700 1050 50  0001 C CNN
	1    3650 3800
	1    0    0    -1  
$EndComp
Text Label 5000 5300 0    60   ~ 0
RoueD
Text Label 5000 5200 0    60   ~ 0
RoueG
$Comp
L +5V #PWR022
U 1 1 50F03F1C
P 7300 1750
F 0 "#PWR022" H 7300 1840 20  0001 C CNN
F 1 "+5V" H 7300 1840 30  0000 C CNN
	1    7300 1750
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR023
U 1 1 50F03B76
P 2350 6400
F 0 "#PWR023" H 2350 6490 20  0001 C CNN
F 1 "+5V" H 2350 6490 30  0000 C CNN
	1    2350 6400
	1    0    0    -1  
$EndComp
$Comp
L CONN_5X2 P1
U 1 1 50F03A7B
P 7700 1950
F 0 "P1" H 7700 2250 60  0000 C CNN
F 1 "Cpt courant tension" V 7700 1950 50  0000 C CNN
	1    7700 1950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR024
U 1 1 50F03245
P 9550 2600
F 0 "#PWR024" H 9550 2690 20  0001 C CNN
F 1 "+5V" H 9550 2690 30  0000 C CNN
	1    9550 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 50F03240
P 9550 3000
F 0 "#PWR025" H 9550 3000 30  0001 C CNN
F 1 "GND" H 9550 2930 30  0001 C CNN
	1    9550 3000
	0    1    1    0   
$EndComp
$Comp
L C C4
U 1 1 50F0322C
P 10900 2800
F 0 "C4" H 10950 2900 50  0000 L CNN
F 1 "100nF" H 10950 2700 50  0000 L CNN
	1    10900 2800
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 50F03225
P 10650 2800
F 0 "C3" H 10700 2900 50  0000 L CNN
F 1 "100nF" H 10700 2700 50  0000 L CNN
	1    10650 2800
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 50F03220
P 10400 2800
F 0 "C2" H 10450 2900 50  0000 L CNN
F 1 "100nF" H 10450 2700 50  0000 L CNN
	1    10400 2800
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 50F0321A
P 10150 2800
F 0 "C1" H 10200 2900 50  0000 L CNN
F 1 "100nF" H 10200 2700 50  0000 L CNN
	1    10150 2800
	1    0    0    -1  
$EndComp
$Comp
L CAPAPOL C7
U 1 1 50F02B16
P 8200 1200
F 0 "C7" H 8250 1300 50  0000 L CNN
F 1 "100µF" H 8250 1100 50  0000 L CNN
	1    8200 1200
	1    0    0    -1  
$EndComp
$Comp
L CAPAPOL C6
U 1 1 50F02B11
P 9450 1200
F 0 "C6" H 9500 1300 50  0000 L CNN
F 1 "100µF" H 9500 1100 50  0000 L CNN
	1    9450 1200
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 50F02ACE
P 9900 1200
F 0 "C5" H 9950 1300 50  0000 L CNN
F 1 "100nF" H 9950 1100 50  0000 L CNN
	1    9900 1200
	1    0    0    -1  
$EndComp
$Comp
L 7805 U1
U 1 1 50F02ABB
P 8850 1050
F 0 "U1" H 9000 854 60  0000 C CNN
F 1 "7805" H 8850 1250 60  0000 C CNN
	1    8850 1050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 50F02784
P 7880 6845
F 0 "#PWR026" H 7880 6845 30  0001 C CNN
F 1 "GND" H 7880 6775 30  0001 C CNN
	1    7880 6845
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 50F0277A
P 7880 6595
F 0 "R2" V 7960 6595 50  0000 C CNN
F 1 "18K" V 7880 6595 50  0000 C CNN
	1    7880 6595
	-1   0    0    1   
$EndComp
$Comp
L R R1
U 1 1 50F02505
P 7880 6045
F 0 "R1" V 7960 6045 50  0000 C CNN
F 1 "10K" V 7880 6045 50  0000 C CNN
	1    7880 6045
	-1   0    0    1   
$EndComp
Text Label 9305 6215 2    60   ~ 0
TX1
NoConn ~ 9400 6315
NoConn ~ 9400 6415
NoConn ~ 9400 6515
NoConn ~ 9400 6615
NoConn ~ 9400 6715
NoConn ~ 9400 6815
$Comp
L GND #PWR027
U 1 1 50F02380
P 9400 6915
F 0 "#PWR027" H 9400 6915 30  0001 C CNN
F 1 "GND" H 9400 6845 30  0001 C CNN
	1    9400 6915
	0    1    1    0   
$EndComp
$Comp
L XBEE P17
U 1 1 50F01654
P 10100 6515
F 0 "P17" H 10100 7115 60  0000 C CNN
F 1 "XBEE" V 10100 6415 50  0000 C CNN
	1    10100 6515
	1    0    0    -1  
$EndComp
Text Label 5000 4200 0    60   ~ 0
TX1
Text Label 5000 4100 0    60   ~ 0
RX1
$Comp
L +5V #PWR028
U 1 1 50F013FF
P 6500 5150
F 0 "#PWR028" H 6500 5240 20  0001 C CNN
F 1 "+5V" H 6500 5240 30  0000 C CNN
	1    6500 5150
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR029
U 1 1 50F013ED
P 6500 4850
F 0 "#PWR029" H 6500 4850 30  0001 C CNN
F 1 "GND" H 6500 4780 30  0001 C CNN
	1    6500 4850
	0    1    1    0   
$EndComp
Text Label 5000 5000 0    60   ~ 0
RESET
Text Label 5000 4900 0    60   ~ 0
TX0
Text Label 5000 4800 0    60   ~ 0
RX0
Text Label 6500 4950 2    60   ~ 0
TX0
Text Label 6500 5050 2    60   ~ 0
RX0
Text Label 6500 4750 2    60   ~ 0
RESET
$Comp
L CONN_5 P18
U 1 1 50F01202
P 7000 4950
F 0 "P18" V 6950 4950 50  0000 C CNN
F 1 "µSD" V 7050 4950 50  0000 C CNN
	1    7000 4950
	1    0    0    -1  
$EndComp
Text Label 2300 4000 2    60   ~ 0
TX3
Text Label 2300 3900 2    60   ~ 0
RX3
$Comp
L +5V #PWR030
U 1 1 50F00E4E
P 3800 900
F 0 "#PWR030" H 3800 990 20  0001 C CNN
F 1 "+5V" H 3800 990 30  0000 C CNN
	1    3800 900 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR031
U 1 1 50F00E4B
P 3500 900
F 0 "#PWR031" H 3500 990 20  0001 C CNN
F 1 "+5V" H 3500 990 30  0000 C CNN
	1    3500 900 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR032
U 1 1 50F00E49
P 3400 900
F 0 "#PWR032" H 3400 990 20  0001 C CNN
F 1 "+5V" H 3400 990 30  0000 C CNN
	1    3400 900 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR033
U 1 1 50F00E45
P 3300 900
F 0 "#PWR033" H 3300 990 20  0001 C CNN
F 1 "+5V" H 3300 990 30  0000 C CNN
	1    3300 900 
	1    0    0    -1  
$EndComp
$EndSCHEMATC
