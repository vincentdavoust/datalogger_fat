/*
** libs.h for project in /home/vincent/c/datalogger_FAT/logparser
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Fri Apr 18 17:53:49 2014 Vincent
** Last update Fri Apr 18 18:46:45 2014 Vincent
*/

#ifndef LIBS_H_
# define LIBS_H_

#include <stdio.h>

# define _PI		3.1415926535
# define _IN_2_M	0.0254

# define ENTRY_SIZE	8

# define VOLT_POS	2
# define CURR_POS	4
# define SPED_POS	6

# define VOLT_VREF	5.0
# define CURR_VREF	3.3
# define CURR_MAX	30
# define CURR_MIN	0
# define FREQ_ECH	50					/* Hz */
# define NB_FENTES	64
# define DIAM_ROUE	24

# define FILTRE_K	10

# define CURR_CAPT	((CURR_MAX - CURR_MIN) / CURR_VREF)	/* Amps per volt */
# define CURR_IREF	(CURR_VREF * CURR_CAPT)

# define VOLT_CONV	(VOLT_VREF / 1024)
# define CURR_CONV	(CURR_IREF / 1024)
# define SPED_CONV	(_PI * DIAM_ROUE * _IN_2_M / NB_FENTES)


#endif /* !LIBS_H_ */
