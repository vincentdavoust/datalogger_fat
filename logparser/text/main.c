/*
** main.cpp for project in /home/vincent/cpp/logparser
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sat Feb 15 18:26:58 2014 Vincent
** Last update Sat Apr 26 14:40:10 2014 Vincent
** 
** Prefix : 
*/

#include "libs.h"

int		main() {
  FILE		*fp;
  char		buf[ENTRY_SIZE + 1];
  int		nb_mes;
  double	tension, courant, vitesse, tmp_v;

  // init();
  fp = fopen("/dev/ttyUSB0", "r");
  if (fp == NULL)
    return (-1);
  nb_mes = 0;
  tension = 0.0;
  courant = 0.0;
  vitesse = 0.0;

  while (1) {
    if ((buf[0] = (char)fgetc(fp)) != EOF) {
      fgets(buf + 1, ENTRY_SIZE + 1, fp);
      // refresh values
      nb_mes = (buf[0] << 8) + buf[1];

      //      printf("\n%c - %c - ", buf[VOLT_POS], buf[VOLT_POS + 1]);
      tension = (double)(buf[VOLT_POS] << 8) + buf[VOLT_POS + 1];
      tension *= VOLT_CONV;

      //      printf("%c - %c - ", buf[CURR_POS], buf[CURR_POS + 1]);
      courant = (double)(buf[CURR_POS] << 8) + buf[CURR_POS + 1];
      courant *= CURR_CONV;

      if (nb_mes % FREQ_ECH) {
	// get impultions per second :
	//	printf("%c - %c\n", buf[SPED_POS], buf[SPED_POS + 1]);
	tmp_v = (double)((buf[SPED_POS] << 8) + buf[SPED_POS + 1]);
	tmp_v *= FREQ_ECH / (nb_mes % FREQ_ECH);
	tmp_v *= SPED_CONV;

	// filter by taking means over FILTRE_K values :
	vitesse = (vitesse * FILTRE_K + tmp_v) / (FILTRE_K + 1);
      }

    }

    printf("\r Tension : %f V - Courant : %f A - Vitesse : %f km/h             \r", tension, courant, vitesse);

    // refresh affichage


    //    sleep(1);

  }

  fclose(fp);

  return (0);
}
