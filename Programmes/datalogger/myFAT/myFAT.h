/*
** myFAT.h for project in /home/vincent/c/myFAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sun Feb  2 19:16:18 2014 Vincent
** Last update Wed Mar  5 18:48:02 2014 Vincent
** 
** Prefix : FAT
*/

#ifndef MYFAT_H_
# define MYFAT_H_

#include <inttypes.h>

# define FAT_APPEND	0
# define FAT_INSERT	1
# define FAT_REPLACE	2

typedef struct {
  uint32_t	totalBlocks;
  uint16_t	bootOffset;
  uint16_t	fatOffset;
  uint16_t	fatEnd;
  uint16_t	rootOffset;
  uint16_t	dataOffset;
  uint16_t	reservedBlocks;
  uint8_t	clusterSize;	/* in blocks */


} FAT_fs_t;

typedef struct {
  unsigned char	block[512];
  FAT_fs_t	disk;
  uint32_t	fileOffset;
  uint32_t	filesize;
  uint32_t	bytesLeft;
  uint32_t	directoryBlock;
  uint32_t	directoryBegin;
  uint32_t	currentBlock;
  uint32_t	byteInBlock;
  uint16_t	currentCluster;
  uint16_t	beginCluster;
  uint8_t	blockInCluster;
  uint8_t	directoryEntry;
  uint8_t	openMode;

} FAT_file_t;


uint8_t		FAT_findFileInDir(FAT_file_t *file, unsigned char *buff,	\
				    unsigned char *filename, uint8_t *entry); /* done */
uint8_t		FAT_identifyBoot(unsigned char *sector);       /* done */
uint8_t		FAT_getNextCluster(FAT_file_t *file);	       /* modifies block too */
uint8_t		FAT_changeFileSize(FAT_file_t *file);
uint8_t		FAT_fopen(FAT_file_t *file, unsigned char *filename, uint8_t modes);
uint8_t		FAT_lseek(FAT_file_t *file, long int pointer);
uint8_t		FAT_fread(FAT_file_t *file, unsigned char *buff, uint32_t len);
uint8_t		FAT_fwrite(FAT_file_t *file, unsigned char *buff, uint32_t len);
uint8_t		FAT_fclose(FAT_file_t *file);

#endif /* !MYFAT_H_ */
