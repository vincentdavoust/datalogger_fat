/*
** myFAT.c for project in /home/vincent/c/myFAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sun Feb  2 19:16:18 2014 Vincent
** Last update Thu May  1 16:09:02 2014 Vincent
** 
** Prefix : FAT
*/
#include "myFAT.h"
#include "../mySD/mySD.h"


uint8_t		FAT_findFileInDir(FAT_file_t *file, unsigned char *buff, unsigned char *filename, uint8_t *entry) {
  uint8_t	i, j, k;
  uint32_t	tmp;
  i = 0; j = 0; k = 0;
  for (i = 0; i < 16; i++) {	/* for all entries on the sector */
    j = 0, k = 0;
    while (buff[(i << 5) + k] == filename[j]) { /* compare caracter of filename of entry and filename */
      printf("same name see : %c=%c", buff[(i << 5) + k] ,filename[j]);

      j++, k++;
      if (filename[j] == '\0' || filename[j] == '.') {
	/* return begin cluster number */
	file->beginCluster = buff[(i << 5) + 0x1A] +	\
		(buff[(i << 5) + 1 + 0x1A] << 8);
	/* return  filesize */
	file->filesize = (buff[(i << 5) + 0x1C]) +	\
	  ((buff[(i << 5) + 1 + 0x1C]) << 8);
	tmp = ((buff[(i << 5) + 0x1C + 2])) +		\
	  ((buff[(i << 5) + 3 + 0x1C]) << 8);
	file->filesize += (tmp << 16);
	*entry = i;
	printf("1 : %d- 2 : %d- 3 : %d- 4 : %d\n",  buff[(i << 5) + 0x1C],  buff[(i << 5) + 0x1C + 1],  buff[(i << 5) + 0x1C + 2],  buff[(i << 5) + 0x1C + 3]);
	printf("\n begin cluster : %d - filesize : %lu\n", file->beginCluster, file->filesize);
	return (0);			/* if filename end, return begin cluster */
      }
      while (buff[(i << 5) + k] == ' ') /* skip spaces */
	k++;
    }

  }
  return (1);

}

uint8_t		FAT_identifyBoot(unsigned char *sector) {
  /* returns 0 if boot sector, 1 if not bs, other if fs error */


  /* Check that it is FAT boot sector by finding "FAT" */
  if (sector[54] != 'F' ||	\
      sector[55] != 'A' ||	\
      sector[56] != 'T')
    return (1);

  /* it is FAT */

  /* check that it is fat16 by finding "16" folowing "FAT" */
  if (sector[57] != '1' ||	\
      sector[58] != '6')
    return (2);

  /* Finaly check that 0x55AA is present.No reason it should not... */
  if (sector[510] != 0x55 || sector[511] != 0xAA) {
    printf("end of sect : %02x, %02x \n", sector[510], sector[511]);
    return (3);
  }

  return (0);
}

uint8_t		FAT_getNextCluster(FAT_file_t *file) {
  uint32_t	tmpBlock0;	/* current block of FAT */
  uint32_t	tmpBlock1;	/* further block of FAT */
  uint16_t	tmpClust;
  uint16_t	i;
  printf("getting next cluster\n fat offset : %d\n", file->disk.fatOffset);
  tmpBlock0 = file->disk.fatOffset + (file->currentCluster >> 8);
  tmpBlock1 = tmpBlock0;
  printf("tmpBlock1 : %d \n", tmpBlock1);
  SD_read(file->block, tmpBlock0, 0, 512);

  /* contenu au cluster precedent */
  tmpClust = file->block[(file->currentCluster & 0xFF) * 2]	\
	     + (file->block[(file->currentCluster & 0xFF) * 2 + 1] << 8);

  if (tmpClust != 0xFFFF && tmpClust > 2) {	/* if a block is already assigned */
    file->currentCluster = tmpClust;
    file->currentBlock =  (tmpClust - 3) * file->disk.clusterSize + file->disk.dataOffset;
    return (0);
  }
  file->currentCluster--;
  /* If a new block has to be allocated : find a free block, then allocate */
  /* detect a sector is unused if its assigned folower in fat is null */
  i = (file->currentCluster & 0xFF) * 2;
  while (1) {
    i += 2;
    if (i > 510) { 		/* cas ou on a depasse le block courant */
      //return (0);
      tmpBlock1 ++;
      SD_read(file->block, tmpBlock1, 0, 512);
    }

    if (file->block[i % 512] == 0 && file->block[(i % 512) + 1] == 0) {      /* free sector found */
      /* get number of found cluster */
      tmpClust = (i >> 1) + (file->currentCluster & 0xFF00);

      /* if only 1 block must be written, write to old sector with adress of next one */
      if (tmpBlock1 == tmpBlock0) {
	file->block[(file->currentCluster & 0xFF) * 2] = tmpClust & 0xFF;
	file->block[(file->currentCluster & 0xFF) * 2 + 1] = tmpClust >> 8;
      }

      /* reserve the newly allocated cluster */
      file->block[i % 512] = 0xFF;
      file->block[(i % 512) + 1] = 0xFF;

      printf("last cluster : %d - new cluster : %d - i : %d\n", file->currentCluster + 1, tmpClust, i);
      printf("block : %02.x %02.x",file->block[(file->currentCluster & 0xFF) * 2], file->block[((file->currentCluster & 0xFF) * 2) + 1]);

      /* write the FAT data */
      SD_write(file->block, tmpBlock1, 0, 512);
      SD_write(file->block, tmpBlock1 + (file->disk.fatEnd - file->disk.fatOffset), 0, 512);


      /* if 2 blocks must be written, re-read and write the first FAT block */
      if (tmpBlock1 != tmpBlock0) {
	/* read the block */
	SD_read(file->block, tmpBlock0, 0, 512);

	/* write in buffer the value of the next cluster at the adress of the precedent */
	file->block[(file->currentCluster & 0xFF) * 2] = i >> 1;
	file->block[((file->currentCluster & 0xFF) * 2) + 1] = (file->currentCluster >> 8) && 0xFF + i >> 9;

	/* write the block */
	SD_write(file->block, tmpBlock0, 0, 512);
	SD_write(file->block, tmpBlock0 + (file->disk.fatEnd - file->disk.fatOffset), 0, 512);
      }



      /* change to next cluster and block */
      file->currentCluster = tmpClust;
      file->currentBlock = tmpClust * file->disk.clusterSize + file->disk.dataOffset;


      return (0);
    }

  }

  return (1);
}

uint8_t		FAT_changeFileSize(FAT_file_t *file) {

  /* just write in directory entry the proper file size */
  SD_read(file->block, file->directoryBlock, 0, 512);

  file->block[(file->directoryEntry << 5) + 0x1c] = file->filesize & 0xFF;
  file->block[(file->directoryEntry << 5) + 0x1d] = (file->filesize >> 8) & 0xFF;
  file->block[(file->directoryEntry << 5) + 0x1e] = (file->filesize >> 16) & 0xFF;
  file->block[(file->directoryEntry << 5) + 0x1f] = (file->filesize >> 24) & 0xFF;

  SD_write(file->block, file->directoryBlock, 0, 512);

  SD_read(file->block, file->currentBlock, 0, 512);

  return (0);
}

uint8_t		FAT_fopen(FAT_file_t *file, unsigned char *filename, uint8_t modes) {
  unsigned char	buff[512];
  uint32_t	i;
  uint8_t	error;

  /* initialize disk */
  SD_init();

  /* get first sector */
  SD_read(buff, 0, 0, 512);

  /* check signature 0xaa55. If none, not valid fs */
  if (buff[510] != 0x55 || buff[511] != 0xAA)
    return (1);

  /* find boot sector (should be first) */
  i = 2040; // gros truc pas beau pour debug
  error = FAT_identifyBoot(buff);
  while (error == 1) {
    SD_read(buff, ++i, 0, 512);
    error = FAT_identifyBoot(buff);
  }
    file->disk.bootOffset = i;

  /* check for errors : if not null and not 1, then bad fs */
  if (error) {
    printf("erreur fs : %u\n", error);
    return (error);
  }

  file->disk.reservedBlocks = buff[0x0e] + (buff[0x0f] << 8);

  /* get fat size, calculate fatOffset & fatEnd */
  file->disk.fatOffset = file->disk.bootOffset + file->disk.reservedBlocks;
  file->disk.fatEnd = file->disk.fatOffset + buff[0x16] + (buff[0x17] << 8);


  /* get fat number, calculate rootOffset */
  file->disk.rootOffset = (file->disk.fatEnd - file->disk.fatOffset) \
				* buff[0x10] + file->disk.fatOffset;

  /* get max entries in root dir, calculate dataOffset. 31 because of cluster 0 and 1 are special */
  file->disk.dataOffset = file->disk.rootOffset + ((buff[0x11] + (buff[0x12] << 8)) >> 4) + 3;

  /* get cluster size */
  file->disk.clusterSize = buff[0x0D];

  /* follow path until within directory  (option, later) */
  file->directoryBegin = file->disk.rootOffset;

  /* get file-> 1st cluster number */
  printf("first car of filename : %c\n", filename[0]);
  i = 0;
  do {
	printf("sector to read : %d\n", file->directoryBegin + i);
    SD_read(buff, file->directoryBegin + i++, 0, 512);
  } while (FAT_findFileInDir(file, buff, filename, &file->directoryEntry));

  /* save cluster of directory (assume dir fits in one cluster for now) */
  file->currentCluster = file->directoryBegin;

  /* save sector adress */
  file->directoryBlock = file->disk.rootOffset + i - 1;

  file->bytesLeft = file->filesize;

  printf("data offset : %d\n", file->disk.dataOffset);
  file->fileOffset = (file->beginCluster - 2) * file->disk.clusterSize	\
		     + file->disk.dataOffset - 3; /* why + 3 ??? */

  file->currentCluster = file->beginCluster;

  /* else create file (later) */


  /* file opened. Set cursor to desired location (according to mode) */

  if (modes == FAT_INSERT || 1) {	/* cursor at start, keep file and relace when writing */
    /* put first sector of file in buffer "block" */
    SD_read(file->block, file->fileOffset, 0, 512);

    /* set read pointers to 0 */
    file->currentBlock = file->fileOffset;
    file->byteInBlock = 0;
    file->blockInCluster = 0;
  }
  else if (modes == FAT_REPLACE && 0) { /* erase file and rewrite */
    /* put first sector of file in buffer "block" */
    SD_read(file->block, file->fileOffset, 0, 512);
    //    for (i = 0; i < 256; i++)
    //file->block[i] = i + 1;

    /* set read pointers to 0 */
    file->currentBlock = file->fileOffset;
    file->byteInBlock = 0;
    file->blockInCluster = 0;

    file->filesize = 0;
    FAT_changeFileSize(file);
    for (i = 0; i < 512; i++) {
      file->block[i] = 0;
    }
    /* set file size to 0 */
    file->filesize = 0;
    FAT_changeFileSize(file);

    /* should erase fat entries corresponding to file (later, but important) */
  }
  else if (modes == FAT_APPEND) { /* go to end of file, then bufferize sector */
    FAT_lseek(file, file->filesize);
    SD_read(file->block, file->currentBlock, 0, 512);
  }


  return (0);
}

uint8_t		FAT_lseek(FAT_file_t *file, long int pointer) {
  /* if place specified, add offset, maybe get next cluster, etc... */
  /* else : read FAT for next cluster, next cluster... until no more specified */
  /* then read last sector until EOF found */
  /* save cluster, sector, byte */

  if (pointer == 0) {		/* pointeur = 0, retour a l'orrigine du fichier */
    file->bytesLeft = file->filesize;
    file->blockInCluster = 0;
    file->currentBlock = file->fileOffset;
    file->byteInBlock = 0;
    file->currentCluster = file->beginCluster;
    SD_read(file->block, file->currentBlock, 0, 512);
  }
  else if (pointer > 0) {
    if (pointer > file->bytesLeft)
      pointer = file->bytesLeft;

    file->bytesLeft -= pointer;

    while (pointer) {
      if (pointer > file->disk.clusterSize * 512) {
	FAT_getNextCluster(file);
	pointer -= 512 * file->disk.clusterSize;
      }
      else if (pointer >	\
	       (file->disk.clusterSize - file->blockInCluster) * 512	\
	       - file->byteInBlock) {
	FAT_getNextCluster(file);
	file->blockInCluster = 0;
	pointer -= (file->disk.clusterSize - file->blockInCluster) * 512 - file->byteInBlock;
      }
      else if (pointer > 512) {
	file->currentBlock++;
	pointer -= 512;
      }
      else if (pointer > 512 - file->byteInBlock) {
	file->currentBlock++;
	pointer -= 512 - file->byteInBlock;
      }
      else {
	file->byteInBlock += pointer;
      }
    }
    // what the hek should be here ? ? ? (file->???)
  }
  else if (pointer < 0 && 0) {	/* not yet implemented */
    if (pointer < file->bytesLeft - file->filesize)
      pointer = file->bytesLeft - file->filesize;

    file->bytesLeft += pointer;

    while (pointer) {
      break;			/* security until loop programmes */

    }
  }

  return (0);
}

uint8_t		FAT_fread(FAT_file_t *file, unsigned char *buff, uint32_t len) {
  /* do not read current sector, use "block" */
  /* while size not reaches, read sector by sector */
  /* return wanted bytes */
  uint32_t	i;

  i = 0;
  /* Load current buffer */

/*  while (file->byteInBlock < 512 && i < len && file->bytesLeft) {
    buff[i++] = file->block[file->byteInBlock++];
    file->bytesLeft--;
  }
*/

  /* load next sector until end of cluster */
  while (len > i && file->bytesLeft) {
    //    file->blockInCluster++;
    if (file->byteInBlock >= 511) {
      file->byteInBlock = 0;
      file->blockInCluster++;
    }
    if (file->blockInCluster >= file->disk.clusterSize) {
      FAT_getNextCluster(file);
      file->blockInCluster = 0;
    }

    if (len - i > 511 && file->bytesLeft > 511) {	/* if full sector must be read */
      SD_read(buff + i, file->currentBlock++, 0, 512);
	  i += 512;
	  file->bytesLeft -= 512;
	  file->blockInCluster++;
    }
    else {				/* if only part of the sector must be read */
      SD_read(file->block, file->currentBlock, 0, 512); /* read full sector to "block" */
      /*  */
      while (file->byteInBlock < 512 && i < len && file->bytesLeft) /* copy wanted bytes only */
		buff[i++] = file->block[file->byteInBlock++];
    }
  }
  if (!file->bytesLeft) {		/* if end of file reached */
    while (i < len)
      buff[i++] = '\0';
    return (1);
         }
  return (0);
}

uint8_t		FAT_fwrite(FAT_file_t *file, unsigned char *buff, uint32_t len) {
  /* write into "block" with proper offset */
  /* write "block" to card */
  /* continue writing data if needed */
  /* finish filling sector with EOFs  */
  uint32_t	i;

  //file->byteInBlock = 0;
  i = 0;
  /* Ecriture de ce qui loge dans le secteur en cours */
  while (len) {
    len--;

    file->block[file->byteInBlock++] = buff[i++];


    if (file->byteInBlock > 511) {

      SD_write(file->block, file->currentBlock, 0, 512);

      file->byteInBlock = 0;
      file->blockInCluster++;
      file->currentBlock++;

      if (file->blockInCluster > file->disk.clusterSize) {
		FAT_getNextCluster(file);
		file->blockInCluster = 0;
		//file->byteInBlock = 0;
      }


      SD_read(file->block, file->currentBlock, 0, 512);
    }


    /* Modification de la taille du fichier */
    if (file->bytesLeft)
      file->bytesLeft--;
    else
      file->filesize++;

  }

  if (file->byteInBlock != 512)
    SD_write(file->block, file->currentBlock, 0, 512);

  if (file->bytesLeft == 0)
    FAT_changeFileSize(file);



  //SD_write(buff, file->currentBlock, 0, 512);

  return (0);
}


uint8_t		FAT_fclose(FAT_file_t *file) {
  /* free stuff, I dunno */
  return (0);
}
