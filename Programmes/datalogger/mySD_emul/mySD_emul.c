/*
** mySD.c for project in /home/vincent/c/myFAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Thu Feb  6 13:40:48 2014 Vincent
** Last update Thu Feb  6 20:27:35 2014 Vincent
** 
** Prefix : SD
*/
#include "mySD_emul.h"
#include <stdio.h>

uint8_t	SD_init() {
  /* open sd clone file */
  SD_file = fopen(SD_FILENAME, "r+");
}

uint8_t	SD_read(unsigned char *buff, uint32_t block,	\
		uint16_t startByte, uint16_t endByte) {
  int	i;
  int	error;

  printf("--sector-- %d --\n", block);

  /* set cursor to equivalent block */
  fseek(SD_file, 512 * block, SEEK_SET);

  /* read 512 bytes */
  for (i = 0; i < 512; i++)
    buff[i] = fgetc(SD_file);

  printf("-- content -- \n");
  for (i = 0; i < 512; i++) {
    printf("%c ", buff[i]);
  }
  printf("\n\n\n");
  return (0);
}

uint8_t	SD_write(unsigned char *buff, uint32_t block,	\
		 uint16_t startByte, uint16_t endByte) {
  int	i;

  /* set cursor to equivalent block */
  fseek(SD_file, block * 512, SEEK_SET);

  /* read 512 bytes */
  for (i = 0; i < 512; i++)
    fputc(buff[i], SD_file);

  return (0);

}
