/*
** mySD.h for project in /home/vincent/c/myFAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Thu Feb  6 13:40:15 2014 Vincent
** Last update Thu Feb  6 17:30:13 2014 Vincent
** 
** Prefix : SD
*/

#ifndef MYSD_H_
# define MYSD_H_

#include <stdio.h>
#include <inttypes.h>

# define SD_FILENAME	"fats/sd_card_b_log"

FILE *SD_file;

uint8_t	SD_init();
uint8_t	SD_read(unsigned char *buff, uint32_t block,	\
		uint16_t startByte, uint16_t endByte);
uint8_t	SD_write(unsigned char *buff, uint32_t block,	\
		uint16_t startByte, uint16_t endByte);

#endif /* !MYSD_H_ */
