/*
** vars.h for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Fri Mar  7 14:59:49 2014 Vincent
** Last update Sun Apr 20 10:58:33 2014 Vincent
*/

#ifndef VARS_H_
#define VARS_H_

#include <inttypes.h>


static unsigned int	SPI_val = 0;
unsigned char	SPI_flag;

unsigned int	valTension;
unsigned int	buffPointer;
unsigned char	*tab_filling;
unsigned char	*tab_sending;
unsigned char	tab1[512];
unsigned char	tab2[512];
unsigned char	*tab_tmp;
unsigned int	measCounter;
unsigned int	pulseCounter;

#endif /* !VARS_H_ */
