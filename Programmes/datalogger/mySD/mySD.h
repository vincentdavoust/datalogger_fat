#ifndef MYSD_H_
# define MYSD_H_

# define DEBUG_STEP	PORTC = i++
#include "../USART/usart2.h"
#include <avr/io.h>
#include <inttypes.h>

typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;

#define SS (1<<PH3)
#define SS_low() (PORTH &= ~SS)// /SS=1
#define SS_high() (PORTH |= SS)//SS=0

//on definit tout les commandes de la carte SD /// note: Au ACMD 0x80+n
#define CMD0	(0)			/* GO_IDLE_STATE */
#define CMD1	(1)			/* SEND_OP_COND (MMC) */
#define	ACMD41	(41)	/* SEND_OP_COND (SDC) */
#define CMD8	(8)			/* SEND_IF_COND */
#define CMD9	(9)			/* SEND_CSD */
#define CMD10	(10)		/* SEND_CID */
#define CMD12	(12)		/* STOP_TRANSMISSION */
#define ACMD13	(13)	/* SD_STATUS (SDC) */
#define CMD16	(16)		/* SET_BLOCKLEN */
#define CMD17	(17)		/* READ_SINGLE_BLOCK */
#define CMD18	(18)		/* READ_MULTIPLE_BLOCK */
#define CMD23	(23)		/* SET_BLOCK_COUNT (MMC) */
#define	ACMD23	(23)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24	(24)		/* WRITE_BLOCK */
#define CMD25	(25)		/* WRITE_MULTIPLE_BLOCK */
#define CMD32	(32)		/* ERASE_ER_BLK_START */
#define CMD33	(33)		/* ERASE_ER_BLK_END */
#define CMD38	(38)		/* ERASE */
#define CMD55	(55)		/* APP_CMD */
#define CMD58	(58)		/* READ_OCR */


DWORD		ocr;
BYTE		sd_buffer[515];
WORD 		sd_cpt;

BYTE		sd_cmd(BYTE cmd, DWORD arg, BYTE crc);
BYTE		sd_response(BYTE cmd);
int			sd_init();
void		sd_read(BYTE *buf, DWORD addr, WORD offset, WORD count);
void		sd_write(BYTE *buf, DWORD addr, WORD count);

void 		spi_init(void);
void 		usart2_spi_init(unsigned int baud);
BYTE 		usart2_spi_data(char data);
void		usart2_interrupt(void);



#endif /* !MYSD_H_ */
