#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include "mySD.h"
#include <util/delay.h>
#include <avr/interrupt.h>



BYTE		sd_cmd(BYTE cmd, DWORD arg, BYTE crc) {
  BYTE		resp;

  /* attente carte prete. quand la carte est prete, elle envoie 0xFF */
  while (cmd != CMD0 && usart2_spi_data(0xFF) != 0xFF);

	printf("\n\n--addr = %lu-\n\r", (arg));

  /* Envoi commande */
  usart2_spi_data(0x40 | cmd); // envoi commande commencant par 0b01xxxxxx
  usart2_spi_data((BYTE)(arg >> 15));
  usart2_spi_data((BYTE)(arg >> 7)); // envoi de arg
  usart2_spi_data((BYTE)(arg << 1));

  usart2_spi_data(0);

/*  usart2_spi_data(1);
  usart2_spi_data(0);
  usart2_spi_data(0);
  usart2_spi_data(0);
*/
  usart2_spi_data(crc | 0b1); // envoi de crc finissant par 0b1

  /* Saisie reponse */
  resp = 0xFF;
  usart2_spi_data(0x00);
  while (resp == 0xFF) // attente de reponse valide
    resp = sd_response(cmd);

  return (resp);
}

BYTE		sd_response(BYTE cmd) {
  BYTE		response;
  response = usart2_spi_data(0); // recuperation de R1
  if ((cmd == CMD58 || cmd ==  CMD8) /*&& response < 2*/) { // recuperation OCR si on le demande
    ocr = usart2_spi_data(0); //  lecture MSO de OCR de R3
    ocr = (ocr << 8) | usart2_spi_data(0);
    ocr = (ocr << 8) | usart2_spi_data(0);
    ocr = (ocr << 8) | usart2_spi_data(0); // lecture LS0
  }

  return (response);
}



int		SD_init(){
  uint8_t	i;
  BYTE		response;
  // attention a CRC en fin d'argument
  _delay_ms(25);// le temps de que l'alim atteint 2.2V + 1ms d'attente necessaire
  usart2_spi_init(120);// init spi
  SS_high();
  for (i = 0; i < 10; i++)
    usart2_spi_data(0xFF); // attendre 80 coups d'horloge

	//_delay_us(50);
  DEBUG_STEP;
  /* envoi commande */
  SS_low();
  response = sd_cmd(CMD0, 0, 0x94); // on reset la card avec CMD0
  SS_high();
  cli();
  DEBUG_STEP;

  /* Verification du succes */
 // if (response != 0x01)//  reponse = 0x01 = ack (idle)
 //   while (1); // sinon echec

 /* en IN Idle State seul CMD0, CMD1, ACMD41,CMD58 and CMD59 sont accept�s */


  // SS_low();
  //response = sd_cmd(CMD8, 0x1AA, 0x86); /* lecture OCR = params de la sd (& voltage) */
   //SS_high();

  /* Verification du succes */
  //if (response > 0x01)//  reponse = 0x01 = ACK
    //return (-2); // sinon echec


  /* Initialisation SD */
  //while (response == 0x01) {
    //SS_low();
    //response = sd_cmd(ACMD41, 0x40000000, 0xFF);// recommander � CMD1 = init
    //SS_high();
 // }

  DEBUG_STEP;
  /* Verification du succes */
//  if (response != 0x00) {//  reponse = 0x00 = ACK
    while (response != 0x00) {
      SS_low();
	  response = sd_cmd(CMD55, 0, 0xFF);
      response = sd_cmd(ACMD41, 0, 0xFF);
      SS_high();
    }

  DEBUG_STEP;
//  }
  // ci-dessus attendre initialisation de la carte


  SS_low();
  response = sd_cmd(CMD58, 0, 0xFF); /* lecture OCR = params de la sd (ccs) */
  SS_high();

  DEBUG_STEP;
  /* Verification du succes */
  if (response != 0x00)//  reponse = 0x00 = ACK
    return (-3); // sinon echec

  DEBUG_STEP;
  /* CCS : block ou octet ? */
  if (ocr >> 30) { // Addressage par blocks
    
  } else { // addressage par octet. forcage des blocks a 512 octets (pr fat)
    response = sd_cmd(CMD16, 0x200, 0xFF);

  }
  usart2_spi_init(500);// augmentation de la frequence a 0.5 MHz 
  return (0);
}


void		SD_read(BYTE *buf, DWORD addr, WORD offset, WORD count) {
  BYTE		response;
  uint16_t	i;

  /* envoi commande lecture */
  SS_low();
  response = sd_cmd(CMD17, addr, 0xFF);
  SS_high();


  SS_low();
//	usart2_spi_data(0);
  /* Verification du succes */
  //if (!response)//  reponse = 0x00 = ACK
    //return ; // sinon echec
	//_delay_us(10);
  
  /* attente data valide */
  while (usart2_spi_data(0xFF) == 0xFF);

  /* lecture data */
  i = 0;
  while (i < 256) {	/* Bloc de 512B suivi d'un CRC de 2B */
    //if (i > offset && i <= (offset + (count >> 2))) {
      buf[2*i] = usart2_spi_data(0xFF);//cr : renplacer les 0x00 avec des 0xFF plus de problemes de collision
	  buf[2*i+1] = usart2_spi_data(0xFF);//cr : renplacer les 0x00 avec des 0xFF plus de problemes de collision
	/*}
    else {
      usart2_spi_data(0xFF);
	  usart2_spi_data(0xFF);
	}*/
	i++;
  }
usart2_spi_data(0xFF);
usart2_spi_data(0xFF);
  SS_high();

if (addr > 2040)
	for (i = 0; i < 512; i++) {
		// printf("%x", buf[i]);
	}

}

void		SD_write(BYTE *buf, DWORD addr, WORD offset, WORD count) {
  BYTE		response;
  uint16_t	i;


  /* Delai de 2 octets */
 // usart2_spi_data(0);
 // usart2_spi_data(0);

  /* mettre en file d'attente le token (octet fixe debutant la trame data) */
  //  sd_buffer[0] = 0xFE;

  /* mise en file d'attente du data : 512 octets */
  /*  for (i = 0; i < count; i++) { // TODO : optimiser, ne pas recopier, utiliser buf directement !
  	sd_buffer[i+1] = buf[i];
  }
  /* mise en file d'attente du CRC CRC */
  //sd_buffer[513] = 0xFF;
  //sd_buffer[514] = 0xFF;


  /* envoi commande ecriture */
  SS_low();
  response = sd_cmd(CMD24, addr, 0xFF);
  SS_high();

  /* Verification du succes */
  if (response)//  reponse = 0x00 = ACK
    return ; // sinon echec



  SS_low();
  usart2_spi_data(0xFE);

  for (i = 0; i < 512; i++) { // TODO : optimiser, ne pas recopier, utiliser buf directement !
  	usart2_spi_data(buf[i]);
  }

  /* CRC : */
  usart2_spi_data(0xFF);
  usart2_spi_data(0xFF);

  /* Envoi sur interruptions */
//  usart2_interrupt(); // initialisation interruptions usart2
//  sd_cpt = 1; // initialisation du curseur d'envoi
//  usart2_spi_data(sd_buffer[0]); // Premier envoi



	// attente reponse
	while (usart2_spi_data(0xFF) == 0xFF);
	while (usart2_spi_data(0xFF) != 0xFF);

  SS_high();

  /*  
		printf("---write :---");
	for (i = 0; i < 512; i++) {
		printf("%x", buf[i]);
	}
  */
}
