#include <avr/interrupt.h>
#include <avr/io.h>
#include "spi.h"

void init_spi(void) {
  SPCR = (1 << SPIE) | (1 << SPE);

}

unsigned int	SPI_receive() {
  unsigned int valeur = 0;

  while (valeur != 0xFF) {
    while (!(SPSR & (1 << SPIF)));
    valeur = SPDR;
  }
  //  printf("0xFF");

    while (!(SPSR & (1 << SPIF)));
    valeur = SPDR << 8;

    while (!(SPSR & (1 << SPIF)));
    valeur |= SPDR;

    return (valeur);
}
