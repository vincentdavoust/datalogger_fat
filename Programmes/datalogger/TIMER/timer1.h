/*
** timer1.h for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Fri Mar  7 14:33:09 2014 Vincent
** Last update Fri Mar  7 15:06:46 2014 Vincent
*/

#ifndef TIMER1_H_
#define TIMER1_H_

void	TMR_initTimer1(void);

#endif /* !TIMER1_H_ */
