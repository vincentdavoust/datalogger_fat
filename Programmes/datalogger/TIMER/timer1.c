/*
** timer1.c for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Fri Mar  7 14:33:02 2014 Vincent
** Last update Fri Mar  7 16:04:24 2014 Vincent
*/

#include <avr/io.h>
#include "timer1.h"

void TMR_initTimer1(void) {
  TCCR1A = 0; // mode CTC
  OCR1A = 20000; // 20ms
  TIMSK1 = 1 << OCIE1A;
  TCNT1 = 0;
  TCCR1B = (1 << WGM12) | (2 << CS10); // mode CTC / prediv=8

}
