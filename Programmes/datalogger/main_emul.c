/*
** test.c for project in /home/vincent/c/myFAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Thu Feb  6 13:53:24 2014 Vincent
** Last update Thu Feb  6 20:36:42 2014 Vincent
** 
** Prefix : 
*/
#include "myFAT/myFAT.h"
#include "mySD_emul/mySD_emul.h"
#include <stdio.h>
#include <string.h>
#include <malloc.h>

int		main () {
  FAT_file_t	myFile;
  unsigned char	buff[512];
  int		i;
  uint8_t	error;

  if (error = FAT_fopen(&myFile, "FILE1.TXT", FAT_INSERT) ) {
    printf("erreur 1 : %u\n", error);
    return (-1);
  }

  if (error = FAT_fread(&myFile, buff, 512)) {
    printf("erreur 2 : %u\n", error);
    return (-2);
  }
  buff[512] = '\0';
  printf("%s", buff);

  if (error = FAT_lseek(&myFile, 0)) {
    printf("erreur 3 : %u\n", error);
    return (-3);
  }
  if (error = FAT_fwrite(&myFile, "I have written", 14)) {
    printf("erreur 4 : %u\n", error);
    return (-4);
  }
  buff[512] = '\0';
  printf("%s", buff);

  if (error = FAT_lseek(&myFile, 0)) {
    printf("erreur 3 : %u\n", error);
    return (-3);
  }
  if (error = FAT_fread(&myFile, buff, 512)) {
    printf("erreur 5 : %u\n", error);
    return (-5);
  }
  buff[512] = '\0';
  printf("%s", buff);


  return (0);
}
