/*
** adc.c for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Fri Mar  7 14:08:54 2014 Vincent
** Last update Fri Mar  7 14:52:21 2014 Vincent
*/

#include "adc.h"
#include "avr/io.h"

void	ADC_init(void) {
  ADMUX = 0; // ADC0
  ADCSRA = (1 << ADEN) | (6 << ADPS0); // 125kHz
  DDRA = 0;
}

unsigned int	ADC_read(void) {
  ADCSRA |= (1<<ADSC);
  while(!(ADCSRA & (1 << ADIF)));
  ADCSRA |= (1<<ADIF);                    // Remet ADIF a 0 en inscrivant 1 !
  return (ADC);
}
