/*
** adc.h for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Fri Mar  7 14:09:17 2014 Vincent
** Last update Fri Mar  7 14:52:13 2014 Vincent
*/

#ifndef ADC_H_
# define ADC_H_

void		ADC_init(void);
unsigned int	ADC_read(void);

#endif /* !ADC_H_ */
