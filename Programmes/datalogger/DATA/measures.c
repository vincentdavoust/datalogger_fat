#include "measures.h"


void	DATA_storeMeasures(unsigned int courant,	\
			   unsigned int tension) {

  if (buffPointer > 511)
    return ;
  tab_filling[buffPointer] = measCounter >> 8;
  fprintf(&out_xbee, "a", tab_filling[buffPointer++]);
  tab_filling[buffPointer] = measCounter & 0xFF;
  fprintf(&out_xbee, "b", tab_filling[buffPointer++]);
  tab_filling[buffPointer] = courant >> 8;
  fprintf(&out_xbee, "c", tab_filling[buffPointer++]);
  tab_filling[buffPointer] = courant & 0xFF;
  fprintf(&out_xbee, "d", tab_filling[buffPointer++]);
  tab_filling[buffPointer] = tension >> 8;
  fprintf(&out_xbee, "e", tab_filling[buffPointer++]);
  tab_filling[buffPointer] = tension & 0xFF;
  fprintf(&out_xbee, "f", tab_filling[buffPointer++]);
  tab_filling[buffPointer] = pulseCounter >> 8;
  fprintf(&out_xbee, "g", tab_filling[buffPointer++]);
  tab_filling[buffPointer] = pulseCounter & 0xFF;
  fprintf(&out_xbee, "h", tab_filling[buffPointer++]);

  fprintf(&out_xbee, "\n");
  if (!(pulseCounter % 50))
    pulseCounter = 0;

  measCounter++;
}
