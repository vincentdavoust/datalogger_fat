#ifndef MEASURES_H_
#define MEASURES_H_

#include <stdio.h>
#include "../vars.h"
#include "../XBEE/xbee.h"

void	DATA_storeMeasures(unsigned int courant,	\
			   unsigned int tension);

#endif /* MEASURES_H_ */
