/*
** main.c for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Thu Feb  6 13:53:24 2014 Vincent
** Last update Thu May  1 08:18:28 2014 Vincent
** 
** Prefix : 
*/

#include <avr/interrupt.h>
#include <avr/io.h>
#include <string.h>
#include "XBEE/xbee.h"
#include "myFAT/myFAT.h"
#include "mySD/mySD.h"
#include "ADC/adc.h"
#include "SPI/spi.h"
#include "USART/usart1.h"
#include "DATA/measures.h"
#include "vars.h"
#include <stdio.h>
#include <string.h>
#include "USART/usart0.h"
#include <util/delay.h>


static FILE out_rs232 = FDEV_SETUP_STREAM(uart0_putchar, NULL, _FDEV_SETUP_WRITE);


ISR (TIMER1_COMPA_vect) {
  printf("%d|", buffPointer);
  DATA_storeMeasures(ADC_read(), valTension);
}

ISR (INT0_vect) {
  pulseCounter++;
}



ISR(SPI_STC_vect) {
  if (SPDR == 0xFF) {
    SPI_flag = 1;
    // flag for beginning of traansmission
  } else if (SPI_flag == 1) { // first byte
    SPI_flag++;
    SPI_val = SPDR << 8;
  } else if (SPI_flag == 2){ // second byte
    SPI_flag = 0;
    SPI_val |= SPDR;
    valTension = SPI_val;
    //    printf("<%u>\r\n", SPI_val);
  }
}

void	open_file(FAT_file_t *file, char *filename) {
  int	i;
  //  char	buff[512];
  for (i = 0; i < 10; i++) {
    printf("filename : %s", filename);
    if (FAT_fopen(file, filename, FAT_INSERT) ) {
      printf("erreur 1 : \n");
      return ;
    }
    return ;
    //FAT_fread(file, buff, 32);
    /* if (buff[0] || buff[1] || buff[8] || buff[9]) {
      filename[7] ++;
      }
    else
    return ;*/
  }
}

int		main () {
  FAT_file_t	myFile;
  uint8_t	error;

  stdout = &out_rs232;

  buffPointer = 0;
  measCounter = 0;
  tab_filling = tab1;
  tab_sending = tab2;
  pulseCounter = 0;


  /* initialisation de la liaison uart pour le printf de debug */
  uart1_init(52); // xbee
  uart0_init(25); // debug (port de l'ancien module)

  /* initialisation adc pour capteur courant */
  ADC_init();

  /* Initialisation SPI pour capteur tension */
  init_spi();

  DDRD |= 1;
  EICRB |= (1 << ISC00);
  EICRB &= ~(1 << ISC01);

  /* ouverture fichier */
  open_file(&myFile, "LOGFILE0.TXT");

  printf("file size : %lu - %lu\n", myFile.filesize, sizeof(myFile.filesize));

  /* initialisation globale des interruptions */
  sei();

  /* initialisation des interruptions timer pour les mesures analogiques */
  TMR_initTimer1();


  /* initialisation des interruptions pour le spi pour les mesures de tension */


  printf("file opened\n");

  /* routine de logs */
  while (1) {
    printf('a');
    /* attente de remplissage du tableau */
    if (buffPointer > 508 /* && (PINJ & 2)*/ ) {
      cli();
      buffPointer = 0;

      /* echange des tableaux tab_sending et tab_filling */
      tab_tmp = tab_filling;
      tab_filling = tab_sending;
      tab_sending = tab_tmp;
      sei();
      printf("\n\n\n");

      /* ecriture datas */
      if (error = FAT_fwrite(&myFile, tab_sending, 512)) {
	printf("erreur 42 : %u\n", error);
	return (-4);
      }
    }
  }

  while (1);
  return (0);
}
