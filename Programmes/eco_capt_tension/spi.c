#include"librairies.h"


void init_SPI(void){                        	//initialisation de la liaison SPI entre le �c et DS1722
    DDRB = DDRB|(1<<PB0)|(1<<PB2)|(1<<PB1);		//SCLK, MOSI en sortie
    DDRB = DDRB & (~(1<<PB1));              	//MISO en entr�e
    DDRA = DDRA|(1<<PA0);

    //    USICR = (1<<USIWM0)|(1<<USICLK);
    //    SPCR = (1<<SPE)|(1<<MSTR)|(1<<CPHA);    	//configuration du MSB first, Fcpu/4, horloge sur front montant et SPI Master
}



char SPI_Transmit_Receive (char cData) {	//fonction qui permet d'envoyer une donn�e : "data" du �c au DS1722
  //  cData = 0xAA;
  //  PORTB |= 1;
  if (cData & 0x80) DO_H(); else DO_L();	/* bit7 */
  CK_H(); CK_L();
  if (cData & 0x40) DO_H(); else DO_L();	/* bit6 */
  CK_H(); CK_L();
  if (cData & 0x20) DO_H(); else DO_L();	/* bit5 */
  CK_H(); CK_L();
  if (cData & 0x10) DO_H(); else DO_L();	/* bit4 */
  CK_H(); CK_L();
  if (cData & 0x08) DO_H(); else DO_L();	/* bit3 */
  CK_H(); CK_L();
  if (cData & 0x04) DO_H(); else DO_L();	/* bit2 */
  CK_H(); CK_L();
  if (cData & 0x02) DO_H(); else DO_L();	/* bit1 */
  CK_H(); CK_L();
  if (cData & 0x01) DO_H(); else DO_L();	/* bit0 */
  CK_H(); CK_L();

  //  PORTB &= 0xFE;
  /*	USIDR = cData;                            	//on met la donn�e "data" dans le registre de transfert SPDR
	USISR = 1 << USIOIF;
    
    do{
		USICR = (1<<USIWM0)|(1<<USICS1)|(1<<USICLK)|(1<<USITC);
    }while(!(USISR&(1<<USIOIF)));              	//on attend que la transmission soit fini pour pouvoir envoyer a nouveau une donn�e
    return USIDR;                            	//retour du registre de transfert
  */
}


char SPI_Transmit_Receive2 (char cData){      	//fonction qui permet d'envoyer une donn�e : "data" du �c au DS1722
  SPI_Transmit_Receive(cData);

  /*	int i;
	USIDR = cData;                            	//on met la donn�e "data" dans le registre de transfert SPDR

    for (i=0; i<8; i++) {
		USICR = (1<<USIWM0)|(1<<USITC);
		USICR = (1<<USIWM0)|(1<<USITC)|(1<<USICLK);
    }              	//on attend que la transmission soit fini pour pouvoir envoyer a nouveau une donn�e
  */
    return USIDR;                            	//retour du registre de transfert
}
