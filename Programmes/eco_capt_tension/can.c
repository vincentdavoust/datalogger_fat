#include"librairies.h"

//////////////////////////////////////////////////////////////////////////

void init_ADC(void)
{
  ADCSRA  = (1<<ADEN);                        // Active la Conversion
  ADCSRA |= (1<<ADPS2)|(0<<ADPS1)|(1<<ADPS0); // Choisir le facteur de division -> ici freq horloge can = 115.2kHZ
  //Registre a adapte pour ce micro c
  ADMUX |= (1<<REFS0) | 5;			//on utilise l'entr�e ADCO
  //utilisation d'une tension de r�ference externe sur le pin AREF
}

//////////////////////////////////////////////////////////////////////////

void init_port(void)
{
  DDRA = 0x00;//0b00000000;        //Port A en Entree
  DDRB = 0xFF;//0b01110101;        //Port B en Sortie
  //  PORTB = 0b00101010;
}

//////////////////////////////////////////////////////////////////////////

unsigned int get_valeur(void)
{
  unsigned int valeur;   					// init des variables

  init_ADC();

  ADCSRA |= (1<<ADSC);                    // D�marre la conversion

  while(!(ADCSRA & (1<<ADIF)));             // attend que la conversion soit finie

  valeur = ADC;                           // enregistre la valeur num�rique de la temp�rature dans valeur

  ADCSRA |= (1<<ADIF);                    // Remet ADIF a 0 en inscrivant 1 !

  return valeur;                   	   // Retourne la valeur num�rique.
}

//////////////////////////////////////////////////////////////////////////
