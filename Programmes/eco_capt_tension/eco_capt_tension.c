#include"librairies.h"

volatile unsigned char flag;

void timer_vect() {
  flag = 1;
}

int main(void)
{
  uint8_t portb;
	unsigned int uiValRetourCan;

	//	init_port();                        // initialisation en E/S du uC
	DDRA = 0x00;
	DDRB = 0xFF;
	/*	while (1) {
	  PORTA = ~PINA;
	  }*/

	init_ADC();                         // initialisation du CAN
	init_SPI();


	flag = 0;
	/*
	// init INT1
	MCUCR = (2 << ISC00);
	GIMSK = (1 << INT1);
	*/
	// init timer
	TMR_attachInt(timer_vect);
	TMR_init0((1 << CTC0), (5 << CS00), 78, (1 << OCIE0A));

	sei();

	DDRA = 0xFF;
	//Boucle principale
	while(1)
	{
		if (flag) {
			uiValRetourCan = get_valeur();                     // r�cup�re la valeur num�rique
			SPI_Transmit_Receive(0xFF);
			if (uiValRetourCan & 0xFF != 0xFF)
			  SPI_Transmit_Receive(uiValRetourCan);
			else
			  SPI_Transmit_Receive(0xFE);
			if (uiValRetourCan >> 8 != 0xFF)
			  SPI_Transmit_Receive(uiValRetourCan >> 8);
			else
			  SPI_Transmit_Receive(0xFE);
			flag = 0;
		}
	}

	return 0;
}
/*
ISR(INT1_vect) {
	flag = 1;
}
*/
