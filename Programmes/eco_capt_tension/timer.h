/*
** timer.h for project in /home/vincent/.../eco_capt_tension/eco_capt_tension
** 
** Made by Vincent Davoust
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sat Dec 14 09:34:58 2013 Vincent Davoust
** Last update Sat Dec 14 10:24:08 2013 Vincent Davoust
** 
** Prefix : TMR
*/

#ifndef TIMER_H_
# define TIMER_H_

void	TMR_init0_default(uint32_t microseconds);
void	TMR_init0(uint8_t tccr0a, uint8_t tccr0b,\
		  uint16_t ocr0, uint8_t timsk);
void	TMR_stop0();
void	TMR_restart0(uint32_t microseconds);
void	(*TMR_interrupt0)();
void	TMR_attachInt(void (*func)());
void	TMR_calcPrediv(uint8_t bits, uint32_t microsseconds,\
		       uint8_t *prediv, uint16_t *ocr);
#endif /* !TIMER_H_ */
