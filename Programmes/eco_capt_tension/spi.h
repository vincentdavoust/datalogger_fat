#ifndef DEF_SPI
#define DEF_SPI

#define DO_H()	PORTB |= (1 << PB0)	/* Set DO "high" */
#define DO_L()	PORTB &= ~(1 << PB0)	/* Set DO "low" */
#define CK_H()	PORTB |= (1 << PB2)	/* Set SCLK "high" */
#define CK_L()	PORTB &= ~(1 << PB2)	/* Set SCLK "low" */

void init_SPI(void);
char SPI_Transmit_Receive (char cData);
char SPI_Transmit_Receive2 (char cData);

#endif
