/*
** timer.c for project in /home/vincent/.../eco_capt_tension/eco_capt_tension
** 
** Made by Vincent Davoust
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sat Dec 14 09:34:48 2013 Vincent Davoust
** Last update Wed Mar 12 15:03:39 2014 Vincent
** 
** Prefix : TMR
*/

#include "librairies.h"

ISR (TIMER0_COMPA_vect) {
  TMR_interrupt0();
}

void		TMR_init0_default(uint32_t microseconds) {
  uint8_t	prediv, tccr0a, timsk;
  uint16_t	ocr0;

  /* default for tccr0a is 16 bits, wafeform generation */
  tccr0a = (1 << TCW0) | (1 << CTC0);
  /* Calculating predi & occr0 */
  TMR_calcPrediv(16, microseconds, &prediv, &ocr0);
  /* default for timsk is interrupt on ocr0 match*/
  timsk = timsk | (1 << OCIE0A);
  TMR_init0(tccr0a, prediv, ocr0, timsk);
}

void	TMR_init0(uint8_t tccr0a, uint8_t tccr0b,\
		  uint16_t ocr0, uint8_t timsk) {
  TCNT0L = 0;				/* Remise a 0 du compteur */
  TCNT0H = 0;
  TCCR0A = tccr0a;			/* Set to requested value */
  TCCR0B = tccr0b;			/* set to requester value */
  OCR0A = ocr0 >> 8;			/* MSB of ocr0 */
  OCR0B = ocr0 & 0xFF;			/* LSB of ocr0 */
  TIMSK = (TIMSK & 0xE4) | timsk;	/* Set only timer0 bits of TIMSK */
}

void	TMR_stop0() {
  TCCR0B = TCCR0B & (~(7 << CS00));	/* dissable clock source */
}

void	TMR_restart0(uint32_t microseconds) {
  uint8_t	prediv;
  uint16_t	ocr0;

  /* Calculating predi & occr0 */
  TMR_calcPrediv(16, microseconds, &prediv, &ocr0);
  OCR0A = ocr0 >> 8;			/* MSB of ocr0 */
  OCR0B = ocr0 & 0xFF;			/* LSB of ocr0 */
  TCCR0B = TCCR0B | prediv;		/* set to requester value */
}

void	TMR_attachInt(void (*func)()) {
  TMR_interrupt0 = func;
}

void	TMR_calcPrediv(uint8_t bits, uint32_t microseconds,\
		       uint8_t *prediv, uint16_t *ocr) {
  uint32_t	maxFlag;
  uint8_t	predivs[5] = {0, 3, 6, 8, 10}; /* predivs, en puissances de 2 */
  uint8_t	i;

  /* Verifications */
  if ((bits != 8 && bits != 16) | !prediv | !ocr)
    return ;

  maxFlag = (bits = 16)? 0xFFFF : 0xFF; /* maximum de ocr, selon nb de bits */

  /* Adaptation de microsecondes par rapport a la frequence cpu en MHz */
  microseconds = microseconds << 2;

  /* Essais avec les differents prediviseurs */
  for (i = 0; i < 5; i++) {
    if ((microseconds >> predivs[i]) < maxFlag) {
      *prediv = i;
      *ocr = (microseconds >> predivs[i]);
      if (maxFlag == 0xFF) {
	*ocr = *ocr << 8;	/* decalage pour aller dans OCR0A (!OCR0B) */
      }
      return ;
    }
  }

  /* Cas ou aucune configuration ne marche : mettre le max */
  *ocr = maxFlag;
  *prediv = 5;
}
